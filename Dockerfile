FROM alpine AS builder
WORKDIR /home/node/
# Copy source files
COPY docker .
# Remove all absolute paths and install dependencies
RUN apk add yarn && \
    sed -i 's/src="\//src="/g' client-latest/index.html && \
    sed -i "s/location.host + '\//location.host + location.pathname + '/g" client-latest/scripts/network.js && \
    sed -i "s/.register('\//.register('/g" client-latest/scripts/ui.js && \
    sed -i "s/'\/s/'s/g; s/'\/i/'i/g" client-latest/service-worker.js && \
    sed -i 's/"start_url": "\//"start_url": "./g; s/"action": "\//"action": "/g' client-latest/manifest.json && \
    yarn install --production

FROM alpine
ENV STUN_SERVER=stun:stun.l.google.com:19302 \
    SINGLE_ROOM=0 \
    PORT=3000 \
    HTTP_PORT=8080
RUN apk add npm lighttpd && \
    rm -rf /etc/lighttpd
WORKDIR /home/node/
COPY --from=builder /home/node/ .
ENTRYPOINT ["/home/node/entrypoint.sh"]
